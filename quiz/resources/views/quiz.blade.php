@extends('layouts.master')

@section('content')
    <h1>Fun Quiz</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="quiz-wrapper">
                    <form action="/quiz/quiz" method="POST">
                        @foreach($name as $n)
                            <b>Question&nbsp;{{ $n->id }}:&nbsp;{{ $n->name }}</b>
                            @foreach($question as $q)
                                @if (($q->quiz_id)==($n->id))
                                    <p>{{ $q->question }}</p>
                                @endif
                                
                            @endforeach
                            
                                @foreach($answer as $a)
                                        @if (($a->question_id)==($n->id))  
                                            <p><input type="radio" name="a{{ $n->id }}" value="{{ $a->correct_answer }}">{{ $a->answer }}</p>
                                        @endif
                                @endforeach
                        @endforeach
                        <button type="submit" class="btn btn-success">Submit Answer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
        $count_true=0;
        $count_false=0;
        for($i = 1; $i <= count($name); $i++){
            if(isset($_POST['a.$i'])){
                $result = $_POST['a.$i'];
                dd($result);
                if($result==1){
                    $count_true++;
                }else{
                    $count_false++;
                }
            }else{
                $result = false;
            }
        }
        echo("Correct Answer is: " . $count_true . "<br>");
        echo("Incorrect Answer is: " . $count_false); 
    ?>
@endsection