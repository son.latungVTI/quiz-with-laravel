<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizs extends Model
{
    public function questions(){
        return $this->hasMany(Questions::class); // Tu dong mapping voi table, co the mo rong
    }
    protected $table = 'quizs';

    public $timestamps = false;
}