<!DOCTYPE html>
<html>
<head>
<title>Quiz made by LTS</title>
<link href="{{asset('public/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('public/css/style.css')}}" type="text/css" rel="stylesheet">
<script src="{{asset('public/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>
</head>
<body>
@yield("content")
</body>
</html>