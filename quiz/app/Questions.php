<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    public function quiz(){
        return $this->belongsTo(Quizs::class);
    }

    public function answers(){
        return $this->hasMany(Answers::class);
    }

    protected $table = 'questions';

    public $timestamps = false;
}
