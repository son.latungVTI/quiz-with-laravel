<?php

use Illuminate\Database\Seeder;

class QuizsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quizs')->insert([
            ['id' => 1, 'name' => 'Soccer'],
            ['id' => 2, 'name' => 'Foods'],
            ['id' => 3, 'name' => 'Traditional'],
            ['id' => 4, 'name' => 'Countries'],
            ['id' => 5, 'name' => 'Animals'],
          ]);
    }
}
