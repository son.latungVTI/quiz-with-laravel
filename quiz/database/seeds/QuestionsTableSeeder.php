<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['id' => 1, 'quiz_id' => 1, 'question' => 'Who is winner of 2018 Ballon dOr?'],
            ['id' => 2, 'quiz_id' => 2, 'question' => 'Globally,Which is the most famous dish of Vietnam?'],
            ['id' => 3, 'quiz_id' => 3, 'question' => 'Sumo is the traditional sport of which country?'],
            ['id' => 4, 'quiz_id' => 4, 'question' => 'Which country has the largest area in the world?'],
            ['id' => 5, 'quiz_id' => 5, 'question' => 'Which animal does symbolize for peace ?'],
          ]);
    }
}
