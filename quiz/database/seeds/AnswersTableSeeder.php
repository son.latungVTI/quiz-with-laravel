<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            ['id' => 1, 'question_id' => 1, 'answer' => 'Cristiano Ronaldo', 'correct_answer' => 0],
            ['id' => 2, 'question_id' => 1, 'answer' => 'Luka Modric', 'correct_answer' => 1],
            ['id' => 3, 'question_id' => 1, 'answer' => 'Lionel Messi', 'correct_answer' => 0],
            ['id' => 4, 'question_id' => 2, 'answer' => 'Pho', 'correct_answer' => 1],
            ['id' => 5, 'question_id' => 2, 'answer' => 'Banh Xeo', 'correct_answer' => 0],
            ['id' => 6, 'question_id' => 2, 'answer' => 'Banh My', 'correct_answer' => 0],
            ['id' => 7, 'question_id' => 3, 'answer' => 'Korea', 'correct_answer' => 0],
            ['id' => 8, 'question_id' => 3, 'answer' => 'Japan', 'correct_answer' => 1],
            ['id' => 9, 'question_id' => 3, 'answer' => 'North Korea', 'correct_answer' => 0],
            ['id' => 10, 'question_id' => 4, 'answer' => 'China', 'correct_answer' => 0],
            ['id' => 11, 'question_id' => 4, 'answer' => 'Canada', 'correct_answer' => 0],
            ['id' => 12, 'question_id' => 4, 'answer' => 'Russia', 'correct_answer' => 1],
            ['id' => 13, 'question_id' => 5, 'answer' => 'Pigeon', 'correct_answer' => 1],
            ['id' => 14, 'question_id' => 5, 'answer' => 'Kangaroo', 'correct_answer' => 0],
            ['id' => 15, 'question_id' => 5, 'answer' => 'Antelope', 'correct_answer' => 0],

          ]);
    }
}
