<?php

namespace App\Http\Controllers;

use App\Quizs;
use App\Questions;
use App\Answers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class QuizsController extends Controller
{
    public function index(){
        return view('index');
    }
    public function start(){
        $name= Quizs::query()->orderBy("id", "ASC")->select('id','name')->get();
        $question= Questions::query()->orderBy("quiz_id", "ASC")->select('quiz_id','question')->get();
        $answer= Answers::query()->orderBy("question_id", "ASC")->select('question_id','answer','correct_answer')->get();
        //$correct_answer = Answers::where("correct_answer", true)->select('correct_answer')->get();
        return \View::make("quiz")->with("name",$name)->with("question",$question)->with("answer",$answer);
    }

    public function store(){
        dd($request->all());
        $result = $request->$result;
    }
}
