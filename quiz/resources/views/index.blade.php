@extends('layouts.master')

@section('content')
    <h1>Fun Quiz</h1>
    <h3>Check your knowledge of the world about Soccer, Foods, Traditional and Countries </h3>
    <button type= "button" id="start" class="btn btn-outline-success">Start Quiz</button>

    <script type="text/javascript">
        $('#start').click(function(){
            window.location.href = "/quiz/quiz";
        })
    </script>
            
@endsection